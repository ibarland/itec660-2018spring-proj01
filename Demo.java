import java.util.*;
import java.io.*;
import java.nio.file.*;

/** Some example functions demonstrating how to use the block-level IO functions of {@link BlockIO}.
 */
class Demo {
    
    /** A sample function which formats &amp; prints out the contents of `fname` to STDOUT,
     * taking `p.BLOCK_SIZE`, `p.RECORD_SIZE`, and `p.LINE_LENGTH` into account.
     */
    static void displayFile( String fname, ParameterSet p ) throws IOException {
        System.out.printf("Contents of %s\n", fname);
        RandomAccessFile f = new RandomAccessFile(fname,"r");
        long n = BlockIO.numBlocks(f,p);
        for (int i=0;  i<n;  ++i) {
            System.out.print( BlockIO.toString( BlockIO.readBlock(f,i,p), p ) );
            }
        f.close();
        }
    static void displayFile( String fname ) throws IOException { displayFile(fname,ParameterSet.DEFAULT); }

    /** A sample function which copies `src` to `target` block-by-block (using `p.BLOCK_SIZE`).
     * It overwrites any existing file named `target`.
     */
    static void copyFile( String src, String target, ParameterSet p ) throws IOException {
        RandomAccessFile in = new RandomAccessFile(src,"r");
        RandomAccessFile out = new RandomAccessFile(target,"rwd");
        long n = BlockIO.numBlocks(in);
        for (int i=0;  i<n;  ++i) {
            BlockIO.writeBlock( out, i, BlockIO.readBlock(in,i,p), p );
            }
        in.close();
        out.setLength(out.getFilePointer());  // Truncate file at the current location.
        out.close();
        }
    static void copyFile( String src, String target ) throws IOException { copyFile(src,target,ParameterSet.DEFAULT); }

    /** A sample function which fills `target` with <em>two</em> copies of `src`, block-by-block (using `p.BLOCK_SIZE`).
     * It overwrites any existing file named `target`.
     */
    static void doubleFile( String src, String target, ParameterSet p ) throws IOException {
        RandomAccessFile in = new RandomAccessFile(src,"r");
        RandomAccessFile out = new RandomAccessFile(target,"rwd");
        long n = BlockIO.numBlocks(in);
        for (int i=0;  i<n;  ++i) {
            byte[] aBlock = BlockIO.readBlock(in,i,p);
            BlockIO.writeBlock( out, i, aBlock, p );
            BlockIO.writeBlock( out, n+i, aBlock, p );  // You can write past the current end-of-file -- it merely grows the file.
            }
        in.close();
        out.setLength(out.getFilePointer());  // Truncate file at the current location.
        out.close();
        }
    static void doubleFile( String src, String target ) throws IOException { doubleFile(src,target,ParameterSet.DEFAULT); }
    
    /** A sample function to create `fname` filled with some random characters,
     *  making it `numBlocks` long (as per `p.BLOCK_SIZE`).
     *  The characters will be 7-bit ascii printable characters
     *    (hence each fitting in a single `byte`, and also <a href="https://en.wikipedia.org/wiki/UTF-8#Description">valid UTF-8</a>).
     *  The sequence of characters generated depend on `p.rng`.
     *  It *overwrites* any existing file named `fname`.
     */
    static void generateRandomFile( String fname, long numBlocks, ParameterSet p ) throws IOException {
        RandomAccessFile f = new RandomAccessFile(fname,"rwd");
        for (long i = 0;  i<numBlocks;  ++i) {
            BlockIO.writeBlock( f, i, randomChars(p.BLOCK_SIZE, p.rng), p );
            }
        f.setLength(f.getFilePointer());  // Truncate file at the current location (in case file had prev. been longer).
        f.close();
        }
    static void generateRandomFile( String fname, long numBlocks ) throws IOException { generateRandomFile( fname, numBlocks, ParameterSet.DEFAULT ); }

    
    /** @return an array of `n` random characters, generated using `rng`.
     * The characters will be 7-bit ascii printable characters 
     * (hence each fitting in a single `byte`, and also <a href="https://en.wikipedia.org/wiki/UTF-8#Description">valid UTF-8</a>).
     */
    static byte[] randomChars( int n, java.util.Random rng ) {
        byte[] data = new byte[n];
        for (int i=0;  i<n;  ++i) { data[i] = (byte)(('a'-XTRA)+rng.nextInt(26+XTRA)); }
        // TODO: update above to call `nextBytes(byte[])`, and then mask each result to ascii.  Compare time.
        // And or, if I only want (say) 6 bits, then generate 64 at a time and distribute into data[].
        return data;
        }
    static byte[] randomChars( int n ) { return randomChars(n, new Random(System.currentTimeMillis())); }
     
    private static int XTRA = 3;  // How many extra characters to use, in addition to 'a'..'z'.  There are 3 okay ones right before 'a'.
    

    /** Create files named `rand1e<i>blocks.txt`, with 10^i blocks, 
     *    for i in [smallestExponent, largestExponent].
     * Creates the blocks with   
     *    new ParameterSet( 4096, 32, new Random(2023))   
     *                    (block-size, record-size, rng).
     * Thus each file's contents start the same [i.e., each file is a prefix of the longest file].
     */
    public static void createRandomBlockFiles(int smallestExponent, int largestExponent ) throws java.io.IOException {
       for (int i=smallestExponent;  i<=largestExponent;  ++i) {
           String fname = "/tmp/rand1e"+i+"blocks.txt";
           System.out.printf( "creating %s...", fname );
           Stats.DEFAULT = new Stats(); // We re-set Stats.DEFAULT each iteration; BlockIO uses it to track #blocks written.
           Stats.DEFAULT.setTimer(fname);
           generateRandomFile( fname, (long)Math.pow(10,i), new ParameterSet( 4096, 32, new Random(2023)) );
           long elapsed = Stats.DEFAULT.timeSince(fname);
           System.out.printf( "done (%dms).  %s\n", elapsed/Stats.NS_PER_MS, Stats.DEFAULT.toString() );
           }
        }



    
    /** Driver. */
    public static void main( String... __ ) throws IOException {
        ParameterSet.DEFAULT = new ParameterSet( 80, 10 ); // block-size, record-size  (both in bytes)

        // Demo the sample functions in this class.
        Stats s = Stats.DEFAULT;
        String initialFile = "sample.txt";
        generateRandomFile( initialFile, 7 );  // 7 blocks, using ParameterSet.DEFAULT: block=80B
        displayFile(initialFile);
        copyFile("sample.txt", "sampleB.txt");
        doubleFile("sampleB.txt", "sample2.txt");
        doubleFile("sample2.txt", "sample4.txt");
        displayFile("sample4.txt");

        // Print stats (for whole program, in this case).
        System.out.println(s.toString());


	Files.delete(Paths.get("sampleB.txt"));  // `Paths.get` just constructs a Path from the String
	Files.delete(Paths.get("sample2.txt"));  //   so that we have the type that `delete` wants.
                                                 // (Arguably, 
	// Leave sample.txt and sample4.txt, for later experimentation.


        createRandomBlockFiles( 0, 4 );
        }
    }
/* #|
@author ibarland
@version 2023-Mar-26

@license: CC-BY 4.0 -- you are free to share and adapt this file
for any purpose, provided you include appropriate attribution.
    https://creativecommons.org/licenses/by/4.0/ 
    https://creativecommons.org/licenses/by/4.0/legalcode 
Including a link to the *original* file satisifies "appropriate attribution".
|# */
