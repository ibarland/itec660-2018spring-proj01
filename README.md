# Overview #

This is the provided, initial code for the itec660 external-sorting project  
    http://www.radford.edu/~itec660/2018spring-ibarland/Homeworks/external-sorting/external-sorting.html

This directory contains code you may use for your project(s), if you like.

Main files:
`Demo.java` -- contains examples of using the `readBlock` and `writeBlock` functions from `BlockIO.java`.  
`BlockIO.java` -- some functions to mimic block-level file access.



### Files you shouldn't need to change (but might want to look at the available methods of) ###
[full javadoc](http://www.radford.edu/~itec660/2018spring-ibarland/Homeworks/external-sorting/initial-code/docs/package-summary.html)

- `BlockIO.java` -- the actual read/write-blocks code.
- `PreferenceSet.java` -- an object that just encapsulates the settings needed by many other functions (e.g. block-size).
- `Stats.java` -- an object to keep track of all the cumulative read/writes, and the their elapsed wall-time.
   It can also time smaller intervals via `setTimer` and `timeSince`.
- `ObjectIan.java` -- (an attempt at what Scala does with its `case class`) -- extending this class means that
   you don't need to write your own boilerplate constructor, `toString`, `equals`, or `hashCode`.
   It assumes that your fields are immutable!
   It is a bit fragile; if it's giving you trouble, just ignore it (don't have your classes extend it,
   and write your own constructor/`toString` and perhaps ignore writing `equals`/`hashCode` if you don't need them
   for a particular class).

- `CommandLineOption.java` -- exports the function `allOptions`, which you can use to get an array
  of your program's options -- taken from the command line, or using a default if not present.
- `CommandLineOptionExample.java` -- an example program showing how to use `CommandLineOption`.
- `QuickSort.java` -- sort a disk-block (that is, sort a byte[], where array is logically composed of records of k bytes).


### Architectural note ###

Many of these provided functions *all* need to know things like the block-size and record-size.
So many methods take a `PreferenceSet` ... BUT if you omit it, then `PreferenceSet.DEFAULT` is used.
This means that you can omit the argument when you want to do "the usual",
but for testing or for specific debugging, you can pass in whatever PreferenceSet you like,
independent of global variables.
[Ditto for `Stats`.]
This approach is sometimes called *dependency injection*.

What this means to you:
Your top-level code will probably want to assign to `ParameterSet.DEFAULT` (with the desired `BLOCK_SIZE`, `RECORD_SIZE`),
and then after that you won't pass these objects around, unless you're doing more detailed testing.
That's what `Demo.main` does.


